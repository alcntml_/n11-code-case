### Project Information 

- `Android Studio` must be 3.6 Beta1 release and above.
- `Gradle version` 3.6.0-beta02
- `Gradle distribution version` gradle-5.6.1-all
- `CompileSdkVersion` 29
- `BuildToolsVersion` 29.0.2
- `MinSdkVersion` 21
- `TargetSdkVersion` 29
- `KotlinVersion` 1.3.50

### Architectural Information 

    (package : di)
- ActivityBuilder : All activity modules opened for Dagger to generate activity must be written in the same format.
- AppComponent : New module opened must be added as module to here.
- AppModule : In this class, we must be created the classes that must be created once anywhere in the project in the same format.

   (package: api)
- ApiHelper : Interface where callbacks of retrofit was created
-ApiHelperImp : Presenter communicates with ApiHelperImp to connect the presenter to the network. It does this with methods that have Observable returns. These methods include the FlatMap scope of the Observable.
In fact, using an intermediate layer to create a change in the response or query after the subscribers to the observable returns to the presenter. This returns the tread required for the rx methods and then returns.
-ExceptionHandle : This is the class required to handle errors returned from the service.


  (sample uı package : main)
- presenter : In order to assign a request to the service, you can access the Observable method of the service you want in the ApiHelper class from the presenter's constructor. Observable method from ApiHelper
In order to make a subscription, you need to add a compositeDisposable based on the base presenter and addSubscription method. Observable return to the presenter for rx
You can use all the necessary methods. For example, MainPresenterImp class.
-....ActivityModule : This module is reporting the classes(like presenter etc.) that will be used on this page to Dagger.

