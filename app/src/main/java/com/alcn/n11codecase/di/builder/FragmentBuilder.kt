package com.alcn.n11codecase.di.builder

import com.alcn.n11codecase.ui.dashboard.DashboardFragmentModule
import com.alcn.n11codecase.ui.dashboard.view.DashboardFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [(DashboardFragmentModule::class)])
    abstract fun bindDashboardFragment(): DashboardFragment

}