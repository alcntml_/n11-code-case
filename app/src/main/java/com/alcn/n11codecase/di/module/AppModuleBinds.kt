package com.alcn.n11codecase.di.module

import com.alcn.n11codecase.app.initializers.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet

@Module
abstract class AppModuleBinds {

    @IntoSet
    @Binds
    abstract fun provideLogger(bind: LoggerInitializer): AppInitializer

    @IntoSet
    @Binds
    abstract fun provideStetho(bind: StethoInitializer): AppInitializer

    @IntoSet
    @Binds
    abstract fun provideFabric(bind: FabricInitializer): AppInitializer
}