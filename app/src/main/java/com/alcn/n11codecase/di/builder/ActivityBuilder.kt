package com.alcn.n11codecase.di.builder


import com.alcn.n11codecase.ui.main.MainActivityModule
import com.alcn.n11codecase.ui.main.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    abstract fun bindMainActivity(): MainActivity

}