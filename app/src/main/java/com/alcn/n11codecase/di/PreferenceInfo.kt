package com.alcn.n11codecase.di

import javax.inject.Qualifier

@Qualifier
@Retention annotation class PreferenceInfo