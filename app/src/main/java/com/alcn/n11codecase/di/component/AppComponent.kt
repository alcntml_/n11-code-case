package com.alcn.n11codecase.di.component

import com.alcn.n11codecase.app.App
import com.alcn.n11codecase.di.builder.ActivityBuilder
import com.alcn.n11codecase.di.builder.FragmentBuilder
import com.alcn.n11codecase.di.module.AppModule
import com.alcn.n11codecase.utility.module.AppUtilModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ActivityBuilder::class,
        FragmentBuilder::class,
        AppUtilModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}