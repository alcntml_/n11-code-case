package com.alcn.n11codecase.di.module

import android.content.Context
import com.alcn.n11codecase.app.App
import com.alcn.n11codecase.data.preferences.AppPreferenceHelper
import com.alcn.n11codecase.data.preferences.PreferenceHelper
import com.alcn.n11codecase.di.ApiKeyInfo
import com.alcn.n11codecase.di.PreferenceInfo
import com.alcn.n11codecase.network.api.ApiHelper
import com.alcn.n11codecase.utility.Constants
import com.alcn.n11codecase.utility.provider.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton


@Module(includes = [AppModuleBinds::class])
class AppModule {

    @Provides
    fun provideContext(application: App): Context = application.applicationContext

    @Provides
    @ApiKeyInfo
    internal fun provideApiKey(): String = "Api Key"

    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = Constants.App.PREF_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper =
        appPreferenceHelper

    @Provides
    @Singleton
    fun providesRemoteService(context: Context): ApiHelper {
        return ApiHelper.createRetrofit(context)
    }

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()
}