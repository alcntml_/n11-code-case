package com.alcn.n11codecase.data.model.response.dashboard

import android.os.Parcelable
import com.alcn.n11codecase.data.model.base.BaseResponse
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemModel(
    @SerializedName("id") val id: Int,
    @SerializedName("value") val value: Int,
    @SerializedName("opened") var opened: Boolean,
    @SerializedName("isVisibile") var isVisibile: Boolean) : Parcelable, BaseResponse()