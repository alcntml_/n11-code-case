package com.alcn.n11codecase.data.model.base

import android.os.Parcel
import android.os.Parcelable

open class BaseResponse : Parcelable {

    private constructor(parcel: Parcel) : this()

    constructor()

    override fun writeToParcel(dest: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BaseResponse> {
        override fun createFromParcel(parcel: Parcel): BaseResponse {
            return BaseResponse(parcel)
        }

        override fun newArray(size: Int): Array<BaseResponse?> {
            return arrayOfNulls(size)
        }
    }
}