package com.alcn.n11codecase.custom.flipcard

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.alcn.n11codecase.R
import com.alcn.n11codecase.data.model.response.dashboard.ItemModel
import com.alcn.n11codecase.utility.Constants
import com.alcn.n11codecase.utility.extension.px

open class FlipCard : RelativeLayout {

    @BindView(R.id.iconCard)
    lateinit var iconCard: CardView
    @BindView(R.id.placeHolderCard)
    lateinit var placeHolderCard: CardView
    @BindView(R.id.contentTV)
    lateinit var contentTV: TextView
    @BindView(R.id.placeholderIV)
    lateinit var placeholderIV: ImageView

    private lateinit var item: ItemModel
    private var onFlipCardListener: OnFlipCardListener? = null
    private var animating = false
    private var hideAnimated = false
    private var showAnimated = false

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        val view = View.inflate(context, R.layout.custom_flipcard_layout, this)
        ButterKnife.bind(this, view)
        clipToPadding = false
        setPaddingRelative(10.px,10.px,10.px,10.px)
        placeHolderCard.isClickable  = true
        iconCard.isClickable = false
    }

    fun setOnFlipCardListener(onFlipCardListener: OnFlipCardListener){
        this.onFlipCardListener = onFlipCardListener
    }

    fun setItem(item: ItemModel) {
        this.item = item
        contentTV.text = this.item.value.toString()

        if (isOpened()){
            placeHolderCard.isClickable  = false
            iconCard.isClickable = true
            placeHolderCard.visibility = View.GONE
            iconCard.visibility = View.VISIBLE
            placeHolderCard.rotationY = Constants.AnimationValues.rotate
            iconCard.rotationY = 0f
        }else{
            iconCard.isClickable  = false
            placeHolderCard.isClickable = true
            iconCard.visibility = View.GONE
            placeHolderCard.visibility = View.VISIBLE
            iconCard.rotationY = Constants.AnimationValues.rotate
            placeHolderCard.rotationY = 0f
        }
    }

    fun getItem(): ItemModel{
        return this.item
    }

    private fun showContent(){
        item.opened = true
        /*animate(placeHolderCard,iconCard)*/
        onFlipCardListener?.onShown(item)
    }

    private fun hideContent(){
        item.opened = false
        /*animate(iconCard,placeHolderCard)*/
        onFlipCardListener?.onHide(item)
    }

    fun showAnimate(){
        if (!showAnimated) {
            animate(placeHolderCard, iconCard, true)
            showAnimated = true
            hideAnimated = false
        }
    }

    fun hideAnimate(){
        if (!hideAnimated) {
            animate(iconCard, placeHolderCard,false)
            showAnimated = false
            hideAnimated = true
        }
    }

    fun removeItem(){
        iconCard.visibility = View.INVISIBLE
        placeHolderCard.visibility = View.INVISIBLE
        iconCard.isClickable  = false
        placeHolderCard.isClickable = false
        item.isVisibile = false
    }

    private fun isOpened(): Boolean{
        return item.opened
    }

    private fun animate(hideView: View, showView: View, isShowing: Boolean){
        if (!animating) {
            animating = true
            hideView.animate()
                .rotationY(Constants.AnimationValues.rotate)
                .setDuration(Constants.AnimationValues.duration)
                .setInterpolator(AccelerateInterpolator())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        hideView.visibility = View.GONE
                        showView.rotationY = Constants.AnimationValues.rotate*-1
                        showView.visibility = View.VISIBLE
                        showView.animate().rotationY(0f).setDuration(Constants.AnimationValues.duration).setListener(null)
                        showView.isClickable = true
                        hideView.isClickable = false
                        animating = false
                        onFlipCardListener?.onFlipAnimationEnd(item,isShowing)
                    }
                })
        }
    }

    @OnClick(R.id.placeHolderCard)
    fun onPlaceHolderClick(){
        showContent()
    }

    @OnClick(R.id.iconCard)
    fun onIconClick(){
        hideContent()
    }

    interface OnFlipCardListener {
        fun onFlipAnimationEnd(item: ItemModel, isShowing: Boolean)
        fun onShown(item: ItemModel)
        fun onHide(item: ItemModel)
    }
}
