package com.alcn.n11codecase.utility

object Constants {

    object App {
        internal const val APP_DB_NAME = "alcn-n11codecase"
        internal const val PREF_NAME = "alcn-n11codecase-pref"
        internal const val EMPTY_EMAIL_ERROR = 1001
        internal const val INVALID_EMAIL_ERROR = 1002
        internal const val EMPTY_PASSWORD_ERROR = 1003
        internal const val LOGIN_FAILURE = 1004
        internal const val NULL_INDEX = -1L

        enum class LoggedInMode constructor(val type: Int) {
            LOGGED_IN_MODE_LOGGED_OUT(0),
            LOGGED_IN_MODE_GOOGLE(1),
            LOGGED_IN_MODE_FB(2),
            LOGGED_IN_MODE_SERVER(3)
        }
    }

    object ArgumentsConstants {
        const val ARG_TAG = "TAG"
        const val ARG_LEVEL = "LEVEL"
    }

    object AnimationValues{
        const val rotate = 90f
        const val duration = 150L
    }

    object GameValues{
        const val lastLevel = 4
    }

    object RequestCode

    object Permission

    object RegExp

}