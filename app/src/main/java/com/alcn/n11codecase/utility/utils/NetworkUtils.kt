package com.alcn.n11codecase.utility.utils

import android.content.Context
import android.net.NetworkInfo
import android.net.ConnectivityManager
import android.os.Build


object NetworkUtils {

    fun isNetworkConnection(context: Context): Boolean {
        val conMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            (conMgr!!.activeNetwork != null)
        } else {
            conMgr?.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)?.state == NetworkInfo.State.CONNECTED ||
                    conMgr?.getNetworkInfo(ConnectivityManager.TYPE_WIFI)?.state == NetworkInfo.State.CONNECTED
        }

    }

}