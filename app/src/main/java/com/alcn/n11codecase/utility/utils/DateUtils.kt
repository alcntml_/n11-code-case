package com.alcn.n11codecase.utility.utils

import android.annotation.SuppressLint
import java.sql.Time
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    private fun getCurrentTime(): Date {
        return Date(Time(System.currentTimeMillis()).time)
    }

    fun getTimeAfterDay(time: Long, day: Int): Date {
        return Date(time + (86400000 * day))
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentHour(): Int {
        val formatter = SimpleDateFormat("HH")
        val strDate = formatter.format(Date(Time(System.currentTimeMillis()).time))
        return strDate.toInt()
    }

    @SuppressLint("SimpleDateFormat")
    fun getHour(time: Long): Int {
        val formatter = SimpleDateFormat("HH")
        val strDate = formatter.format(time)
        return strDate.toInt()
    }

    @SuppressLint("SimpleDateFormat")
    fun getMinute(time: Long): Int {
        val formatter = SimpleDateFormat("mm")
        val strDate = formatter.format(time)
        return strDate.toInt()
    }

    fun hourFromLong(hour: Long): Double {
        return (hour.toDouble() / 3600000)
    }

    fun hourToLong(hour: Int): Long {
        return (hour * 3600000).toLong()
    }

    fun minuteToLong(minute: Int): Long {
        return (minute * 60000).toLong()
    }

    fun hourToString(hour: Long): String {
        return (hour / 3600000).toString()
    }

    fun minuteToString(minute: Long): String {
        return (minute / 60000).toString()
    }

    @SuppressLint("SimpleDateFormat")
    fun getMonth(time: Long): Int {
        return SimpleDateFormat("MM").format(Date(time)).toInt()
    }

    @SuppressLint("SimpleDateFormat")
    fun getDayOfMonth(time: Long): Int {
        return SimpleDateFormat("dd").format(Date(time)).toInt()
    }

    @SuppressLint("SimpleDateFormat")
    fun getDayOfWeek(time: Long, locale: Locale?): String {
        if (locale != null) {
            return SimpleDateFormat("EEE", locale).format(Date(time))
        }
        return SimpleDateFormat("EEE").format(Date(time))
    }

    @SuppressLint("SimpleDateFormat")
    fun getAsFormat(time: Long, format: String, locale: Locale?): String {
        if (locale != null) {
            return SimpleDateFormat(format, locale).format(Date(time))
        }
        return SimpleDateFormat(format).format(Date(time))
    }

    fun isPM(): Boolean {
        return getCurrentHour() >= 12
    }

    fun isAM(): Boolean {
        return getCurrentHour() < 12
    }

    fun isAfternoon(time: Long, locale: Locale?): Boolean {
        val calendar: Calendar =
            if (locale != null) Calendar.getInstance(locale) else Calendar.getInstance()
        calendar.timeInMillis = time

        return calendar.get(Calendar.DAY_OF_WEEK) == 1 || calendar.get(Calendar.DAY_OF_WEEK) == 7
    }

    fun isMorning(): Boolean {
        return (getCurrentHour() in 6..11)
    }

    fun isDay(): Boolean {
        return (getCurrentHour() in 12..19)
    }

    fun isEvening(): Boolean {
        return (getCurrentHour() in 20..24)
    }

    fun isNight(): Boolean {
        return getCurrentHour() < 6
    }

    fun isToday(time: Long): Boolean {
        if (getMonth(time) != getMonth(
                getCurrentTime().time
            )
        ) {
            return false
        }

        return getDayOfMonth(time) == getDayOfMonth(
            getCurrentTime().time
        )
    }

    @SuppressLint("SimpleDateFormat")
    fun getWithouthourAndMinute(time: Long): Long {
        val formatter = SimpleDateFormat("dd/MM/yyyy")
        val strDate = formatter.format(time)

        return formatter.parse(strDate).time
    }
}