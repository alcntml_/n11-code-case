package com.alcn.n11codecase.utility.extension

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager

/**
 * The extension brings the screen width. Return type is integer
 */
fun Context?.getScreenWidth(): Int {
    if (this.isNull()) return 0
    val windowManager = this!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    windowManager.let {
        val dm = DisplayMetrics()
        it.defaultDisplay.getMetrics(dm)
        return dm.widthPixels
    }
}

/**
 * The extension brings the screen height. Return type is integer
 */
fun Context?.getScreenHeight(): Int {
    if (this.isNull()) return 0
    val windowManager = this!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    windowManager.let {
        val dm = DisplayMetrics()
        it.defaultDisplay.getMetrics(dm)
        return dm.heightPixels
    }
}