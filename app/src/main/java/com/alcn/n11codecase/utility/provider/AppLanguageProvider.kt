package com.alcn.n11codecase.utility.provider

import android.content.Context
import com.alcn.n11codecase.data.preferences.AppPreferenceHelper
import javax.inject.Inject

class AppLanguageProvider @Inject constructor() {
    private var appLanguage: String? = null


    fun getAppLanguage(): String {
        return if (appLanguage != null) appLanguage!! else ""
    }

    fun getAppLanguage(context: Context): String {
        if (appLanguage == null) {
            appLanguage = AppPreferenceHelper(
                context,
                AppPreferenceHelper.PREF_KEY_CURRENT_LANGUAGE
            ).getAppLanguage()
        }
        return when (appLanguage) {
            ENGLISH_CODE -> ENGLISH
            ARABIC_CODE -> ARABIC
            else -> DEFAULT_LANG
        }
    }

    fun checkLanguageSupport(language: String): Boolean {
        return availableLanguageCodes.contains(language)
    }

    companion object {
        const val ENGLISH = "English"
        const val ARABIC = "Arabic"
        const val ENGLISH_CODE = "en"
        const val ARABIC_CODE = "ar"
        const val DEFAULT_LANG = ARABIC_CODE

        private var appLanguageProvider: AppLanguageProvider? = null

        val instance: AppLanguageProvider
            get() {
                if (appLanguageProvider == null) {
                    appLanguageProvider = AppLanguageProvider()
                }
                return appLanguageProvider as AppLanguageProvider
            }

        private val availableLanguages: List<String>
            get() {
                val languages: ArrayList<String> = ArrayList()
                languages.add(ENGLISH)
                languages.add(ARABIC)
                return languages
            }

        private val availableLanguageCodes: List<String>
            get() {
                val languages: ArrayList<String> = ArrayList()
                languages.add(ENGLISH_CODE)
                languages.add(ARABIC_CODE)
                return languages
            }
    }
}