package com.alcn.n11codecase.utility.module

import com.alcn.n11codecase.utility.provider.AppLanguageProvider
import com.alcn.n11codecase.utility.provider.BusProvider
import dagger.Module
import dagger.Provides

@Module
class AppUtilModule {

    @Provides
    internal fun provideAppLanguageProvider(): AppLanguageProvider =
        AppLanguageProvider()

    @Provides
    internal fun provideBusProvider(): BusProvider = BusProvider()
}