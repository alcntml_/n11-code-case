package com.alcn.n11codecase.utility.wrapper

import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import android.os.LocaleList
import com.alcn.n11codecase.utility.provider.AppLanguageProvider
import java.util.*

class AppContextWrapper(base: Context?) : ContextWrapper(base) {
    fun wrap(context: Context, appLang: String): ContextWrapper {
        var context1 = context
        val resources = context1.resources
        val configuration = resources.configuration

        val locale: Locale = when (appLang) {
            AppLanguageProvider.ARABIC_CODE -> Locale(AppLanguageProvider.ARABIC_CODE)
            AppLanguageProvider.ENGLISH_CODE -> Locale(AppLanguageProvider.ENGLISH_CODE)
            else -> Locale(AppLanguageProvider.ENGLISH_CODE)
        }
        Locale.setDefault(locale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(locale)

            val localeList = LocaleList(locale)
            LocaleList.setDefault(localeList)
            configuration.setLocales(localeList)

            context1 = context1.createConfigurationContext(configuration)
        } else {
            configuration.setLocale(locale)
            resources.updateConfiguration(configuration, resources.displayMetrics)
        }
        return ContextWrapper(context1)
    }
}