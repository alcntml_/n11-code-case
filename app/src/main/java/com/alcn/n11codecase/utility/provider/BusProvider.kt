package com.alcn.n11codecase.utility.provider

import android.os.Handler
import com.squareup.otto.Bus
import com.squareup.otto.ThreadEnforcer

class BusProvider {

    companion object {
        private var bus: LdsBus? = null

        fun getInstance(): LdsBus {
            if (bus == null) {
                bus = BusProvider().LdsBus()
            }

            return bus!!
        }

    }

    inner class LdsBus : Bus(ThreadEnforcer.ANY) {

        fun postDelayed(event: Any, delayMillis: Long) {

            Handler().postDelayed({ post(event) }, delayMillis)
        }
    }
}