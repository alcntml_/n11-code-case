package com.alcn.n11codecase.app.initializers

import android.app.Application

interface AppInitializer {
    fun init(application: Application)
}
