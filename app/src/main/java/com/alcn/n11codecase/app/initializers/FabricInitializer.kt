package com.alcn.n11codecase.app.initializers

import android.app.Application
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import javax.inject.Inject

class FabricInitializer @Inject constructor() : AppInitializer {
    override fun init(application: Application) {
        Fabric.with(application, Crashlytics())
    }
}
