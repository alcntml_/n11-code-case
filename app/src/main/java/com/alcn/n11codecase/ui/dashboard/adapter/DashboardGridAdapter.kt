package com.alcn.n11codecase.ui.dashboard.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alcn.n11codecase.R
import com.alcn.n11codecase.custom.flipcard.FlipCard
import com.alcn.n11codecase.data.model.response.dashboard.ItemModel
import com.alcn.n11codecase.utility.Constants

class DashboardGridAdapter(
    private val context: Context,
    private val gameList: ArrayList<ItemModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var onGameListener: OnGameListener? = null
    private var recyclerView: RecyclerView? = null
    var shownCards = ArrayList<ItemModel>()
    var isStarted = false
    private var  isFinished = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DashboardGridViewHolder(
            LayoutInflater.from(parent.context)!!.inflate(
                R.layout.item_flipcard,
                parent,
                false
            )
        )
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun getItemCount(): Int {
        return gameList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val dashboardGridViewHolder: DashboardGridViewHolder = holder as DashboardGridViewHolder
        val itemModel = gameList[position]
        dashboardGridViewHolder.setItem(gameList[position],holderInteractor)

        if (isStarted) {
            if (!itemModel.isVisibile) {
                dashboardGridViewHolder.flipCard.visibility = View.INVISIBLE
            } else {
                dashboardGridViewHolder.flipCard.visibility = View.VISIBLE
                if (itemModel.opened) {
                    dashboardGridViewHolder.flipCard.showAnimate()
                } else {
                    dashboardGridViewHolder.flipCard.hideAnimate()
                }
            }
        }
        enableClick()
        if (isGameDone() && !isFinished){
            isFinished = true
            onGameListener!!.onGameFinished()
        }
    }

    private var holderInteractor = object: DashboardGridViewHolder.HolderInteractor{
        override fun onFlipAnimationEnd(item: ItemModel, isShowing: Boolean) {
            if (isShowing){
                disableClick()
                Handler().postDelayed({
                    control()
                },Constants.AnimationValues.duration*2)
            }
        }

        override fun onShown(item: ItemModel) {
            isStarted = true
            shownCards.add(item)
        }

        override fun onHide(item: ItemModel) {
            isStarted = true
            shownCards.remove(item)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun  disableClick(){
        this.recyclerView!!.setOnTouchListener({ v, event -> true })
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun enableClick(){
        this.recyclerView!!.setOnTouchListener(null)
    }

    private fun control(){
        if (shownCards.size == 0){
            return
        }else if (shownCards.size == 1){
            return
        }else if (shownCards.size == 2){
            if (cardsHasSameValue()){
                for (i in 0 until shownCards.size){
                    /*shownCards[i].isVisibile = false*/
                    val pos = getItemPosition(shownCards[i])
                    gameList[pos].isVisibile = false
                    notifyItemChanged(pos)
                }
            }else{
                for (i in 0 until shownCards.size){
                    /*shownCards[i].opened = false*/
                    val pos = getItemPosition(shownCards[i])
                    gameList[pos].opened = false
                    notifyItemChanged(pos)
                }
            }
            shownCards.clear()
        }else{
            for (i in 0 until shownCards.size){
                /*shownCards[i].opened = false*/
                val pos = getItemPosition(shownCards[i])
                gameList[pos].opened = false
                shownCards.removeAt(i)
                notifyItemChanged(pos)
            }
            shownCards.clear()
        }
    }

    private fun getItemPosition(item: ItemModel): Int{
        for (i in 0..gameList.size){
            if (item.id == gameList[i].id){
                return i
            }
        }
        return -1
    }

    private fun cardsHasSameValue(): Boolean{
        return shownCards[0].value == shownCards[1].value
    }

    private fun isGameDone(): Boolean{
        var doneCount = 0
        for (i in 0 until gameList.size){
            if (!gameList[i].isVisibile){
                doneCount++
            }
        }
        return doneCount == gameList.size
    }

    fun setGameListener(onGameListener: OnGameListener){
        this.onGameListener = onGameListener
    }

    interface OnGameListener{
        fun onGameFinished()
    }

    class DashboardGridViewHolder(
        containerView: View
    ) : RecyclerView.ViewHolder(containerView) ,FlipCard.OnFlipCardListener {
        var flipCard: FlipCard = containerView.findViewById(R.id.flipCard)
        private var itemModel: ItemModel? = null
        private var holderInteractor: HolderInteractor? = null

        fun setItem(item: ItemModel, holderInteractor: HolderInteractor){
            this.itemModel = item
            this.holderInteractor = holderInteractor
            flipCard.setItem(this.itemModel!!)
            flipCard.setOnFlipCardListener(this)
        }

        override fun onFlipAnimationEnd(item: ItemModel, isShowing: Boolean) {
            holderInteractor?.onFlipAnimationEnd(itemModel!!, isShowing)
        }

        override fun onShown(item: ItemModel) {
            flipCard.showAnimate()
            holderInteractor?.onShown(itemModel!!)
        }

        override fun onHide(item: ItemModel) {
            flipCard.hideAnimate()
            holderInteractor?.onHide(itemModel!!)
        }

        interface HolderInteractor{
            fun onFlipAnimationEnd(item: ItemModel, isShowing: Boolean)
            fun onShown(item: ItemModel)
            fun onHide(item: ItemModel)
        }
    }

}