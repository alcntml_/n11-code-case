package com.alcn.n11codecase.ui.base.presenter

import com.alcn.n11codecase.ui.base.view.BaseView

interface BasePresenter<V : BaseView> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

    fun isUserLoggedIn(): Boolean

    fun performUserLogout()


}