package com.alcn.n11codecase.ui.main.presenter


import android.os.CountDownTimer
import com.alcn.n11codecase.data.preferences.PreferenceHelper
import com.alcn.n11codecase.network.api.ApiHelperImp
import com.alcn.n11codecase.ui.base.presenter.BasePresenterImp
import com.alcn.n11codecase.ui.main.view.MainView
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenterImp<V : MainView> @Inject internal constructor(
    disposable: CompositeDisposable, apiHelperImp: ApiHelperImp, preferenceHelper: PreferenceHelper
) : BasePresenterImp<V>(
    compositeDisposable = disposable,
    apiHelperImp = apiHelperImp,
    preferenceHelper = preferenceHelper
), MainPresenter<V> {

    override fun startTimer() {
        timer.start()
    }

    override fun stopTimer() {
        timer.cancel()
    }

    private var timer = object : CountDownTimer(180 * 1000, 1000) {
        override fun onFinish(){
            /*getView()!!.setTimer(0)
            getView()!!.gameOver()*/
        }
        override fun onTick(millisUntilFinished: Long) {
            getView()!!.setTimer((millisUntilFinished / 1000).toInt())
            if ((millisUntilFinished / 1000).toInt() == 0){
                getView()!!.setTimer(0)
                getView()!!.gameOver()
            }
        }
    }
}