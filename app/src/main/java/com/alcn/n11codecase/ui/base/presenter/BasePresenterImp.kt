package com.alcn.n11codecase.ui.base.presenter

import com.alcn.n11codecase.data.preferences.PreferenceHelper
import com.alcn.n11codecase.network.api.ApiHelperImp
import com.alcn.n11codecase.ui.base.view.BaseView
import com.alcn.n11codecase.utility.Constants
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenterImp<V : BaseView> internal constructor(
    protected val compositeDisposable: CompositeDisposable,
    protected val apiHelperImp: ApiHelperImp,
    protected val preferenceHelper: PreferenceHelper
) : BasePresenter<V> {

    private var view: V? = null
    private val isViewAttached: Boolean get() = view != null


    override fun onAttach(view: V?) {
        this.view = view
    }

    override fun getView(): V? = view

    override fun onDetach() {
        compositeDisposable.dispose()
        view = null
    }

    override fun isUserLoggedIn() =
        this.preferenceHelper.getCurrentUserLoggedInMode() != Constants.App.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type

    override fun performUserLogout() = preferenceHelper.let {
        it.setCurrentUserId(null)
        it.setAccessToken(null)
        it.setCurrentUserEmail(null)
        it.setCurrentUserLoggedInMode(Constants.App.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT)
    }

    fun addSubscription(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

}