package com.alcn.n11codecase.ui.main.presenter

import com.alcn.n11codecase.ui.base.presenter.BasePresenter
import com.alcn.n11codecase.ui.main.view.MainView

interface MainPresenter<V : MainView> : BasePresenter<V> {
    fun startTimer()
    fun stopTimer()
}