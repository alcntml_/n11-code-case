package com.alcn.n11codecase.ui.base.view

interface BaseFragmentView : BaseView {
    fun isActive(): Boolean
}