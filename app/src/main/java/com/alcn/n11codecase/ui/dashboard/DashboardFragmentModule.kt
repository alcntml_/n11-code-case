package com.alcn.n11codecase.ui.dashboard

import com.alcn.n11codecase.ui.dashboard.presenter.DashboardPresenter
import com.alcn.n11codecase.ui.dashboard.presenter.DashboardPresenterImp
import com.alcn.n11codecase.ui.dashboard.view.DashboardView
import dagger.Module
import dagger.Provides

@Module
class DashboardFragmentModule{
    @Provides
    internal fun provideDashboardPresenter(dashboardPresenter: DashboardPresenterImp<DashboardView>)
            : DashboardPresenter<DashboardView> = dashboardPresenter
}