package com.alcn.n11codecase.ui.base.view

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.ViewGroup
import com.alcn.n11codecase.data.preferences.AppPreferenceHelper
import com.alcn.n11codecase.utility.provider.AppLanguageProvider
import com.alcn.n11codecase.utility.utils.CommonUtil
import com.alcn.n11codecase.utility.wrapper.AppContextWrapper
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity(), BaseView {

    private var progressDialog: ProgressDialog? = null
    open var rootView: ViewGroup? = null

    abstract fun onCreateActivity(savedInstanceState: Bundle?): ViewGroup

    abstract fun bindView()

    abstract fun onDetachDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        performDI()
        super.onCreate(savedInstanceState)
        setToolbar()
        rootView = onCreateActivity(savedInstanceState)
        bindView()
    }

    private fun setToolbar() {
        /*AppBarUtils.initStatusBar(this.window, this)*/
    }

    override fun hideProgress() {
        progressDialog?.let { if (it.isShowing) it.cancel() }
    }

    override fun showProgress() {
        hideProgress()
        progressDialog = CommonUtil.showLoadingDialog(this)
    }

    private fun performDI() = AndroidInjection.inject(this)


    override fun attachBaseContext(newBase: Context) {
        val appLang = AppLanguageProvider.instance.getAppLanguage()
        super.attachBaseContext(
            AppContextWrapper(newBase).wrap(
                newBase,
                if (appLang.isEmpty()) AppPreferenceHelper(
                    newBase,
                    AppPreferenceHelper.PREF_KEY_CURRENT_LANGUAGE
                ).getAppLanguage()!! else appLang
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        onDetachDisposable()
    }
}