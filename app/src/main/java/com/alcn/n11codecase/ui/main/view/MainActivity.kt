package com.alcn.n11codecase.ui.main.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.alcn.n11codecase.R
import com.alcn.n11codecase.data.event.EndGameEvent
import com.alcn.n11codecase.ui.base.view.BaseFragmentActivity
import com.alcn.n11codecase.ui.dashboard.view.DashboardFragment
import com.alcn.n11codecase.ui.main.presenter.MainPresenterImp
import com.alcn.n11codecase.utility.provider.BusProvider
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseFragmentActivity(), MainView {

    @Inject
    lateinit var mMainPresenter: MainPresenterImp<MainView>

    private var currentTime: Int? = 0

    override fun onCreateActivity(savedInstanceState: Bundle?): ViewGroup {
        setContentView(R.layout.activity_main)
        mMainPresenter.onAttach(this)
        return rootMainActivity as ViewGroup
    }

    override var frameLayoutId: Int
        get() = R.id.fragmentFL
        set(value) {}

    override fun bindView() {
        setSupportActionBar(toolbar)
        BusProvider.getInstance().register(this)
        initView(DashboardFragment.newInstance(1))
        mMainPresenter.startTimer()
    }

    @SuppressLint("SetTextI18n")
    override fun setTimer(time: Int) {
        timerTV.text = "${time}sn"
        currentTime = time
    }

    override fun gameOver() {
        showDialog(getString(R.string.game_over_text))
        mMainPresenter.stopTimer()
    }

    @Subscribe
    fun onEndGameEvent(endGameEvent: EndGameEvent){
        val score = currentTime?.times(3)
        scoreTV.text = score.toString()
        showDialog(getString(R.string.win_game_text)+score)
        mMainPresenter.stopTimer()
    }

    private fun showDialog(message: String){
        val builder = AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
        builder.setTitle(getString(R.string.popup_title))
        builder.setMessage(message)
        builder.setPositiveButton(getString(R.string.popup_button_text),
            { dialog, which ->
                recreate()
                dialog.dismiss()
            })
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()
    }

    override fun onDetachDisposable() {
        mMainPresenter.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

}
