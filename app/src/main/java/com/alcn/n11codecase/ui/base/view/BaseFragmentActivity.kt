package com.alcn.n11codecase.ui.base.view

import android.os.Handler

abstract class BaseFragmentActivity : BaseActivity() {

    protected abstract var frameLayoutId: Int

    //Frame Layout
    fun initView(fragment: BaseFragment) {
        Handler().postDelayed({
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(frameLayoutId, fragment).commitAllowingStateLoss()
        }, 50)

    }

    fun clearStack() {
        val transaction = supportFragmentManager.beginTransaction()
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack()
        }
    }

    fun changeFragment(fragment: BaseFragment) {
        Handler().postDelayed({
            //getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.to_right_animation, R.anim.from_left_animation, R.anim.from_right_animation, R.anim.to_left_animation).replace(R.id.frameLayout_baseActivity, fragment).addToBackStack(fragment.toString()).commit();
            supportFragmentManager.beginTransaction().replace(frameLayoutId, fragment).commit()
        }, 200)
    }
}
