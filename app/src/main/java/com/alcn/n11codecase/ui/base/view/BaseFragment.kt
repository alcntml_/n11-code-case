package com.alcn.n11codecase.ui.base.view

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import butterknife.ButterKnife
import com.alcn.n11codecase.utility.utils.CommonUtil
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment : Fragment(), BaseFragmentView {

    var parentActivity: BaseFragmentActivity
        get() = (activity!! as BaseFragmentActivity)
        set(value) {}

    private var progressDialog: ProgressDialog? = null
    private var rootView: View? = null

    override fun isActive(): Boolean {
        return isAdded
    }

    abstract fun getLayoutId(): Int

    abstract fun bindScreen()

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (rootView == null) {
            rootView = inflater.inflate(getLayoutId(), container, false)
            ButterKnife.bind(this, rootView!!)
            bindScreen()
        }
        return rootView
    }

    override fun hideProgress() {
        if (progressDialog != null && progressDialog?.isShowing!!) {
            progressDialog?.cancel()
        }
    }

    override fun showProgress() {
        hideProgress()
        progressDialog = CommonUtil.showLoadingDialog(this.context)
    }

    fun getBaseActivity() = parentActivity

    private fun performDependencyInjection() = AndroidSupportInjection.inject(this)

    interface CallBack {
        fun onFragmentAttached()
        fun onFragmentDetached(tag: String)
    }
}