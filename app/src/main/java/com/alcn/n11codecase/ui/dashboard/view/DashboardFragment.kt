package com.alcn.n11codecase.ui.dashboard.view

import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import com.alcn.n11codecase.R
import com.alcn.n11codecase.data.event.EndGameEvent
import com.alcn.n11codecase.ui.base.view.BaseFragment
import com.alcn.n11codecase.ui.dashboard.adapter.DashboardGridAdapter
import com.alcn.n11codecase.ui.dashboard.presenter.DashboardPresenterImp
import com.alcn.n11codecase.utility.Constants
import com.alcn.n11codecase.utility.Constants.ArgumentsConstants.ARG_LEVEL
import com.alcn.n11codecase.utility.provider.BusProvider
import javax.inject.Inject

class DashboardFragment : BaseFragment(), DashboardView, DashboardGridAdapter.OnGameListener {

    @BindView(R.id.dashboardRecyclerV)
    lateinit var dashboardRecyclerV: RecyclerView

    private var level: Int? = 1

    @Inject
    lateinit var mDashboardPresenter: DashboardPresenterImp<DashboardView>

    companion object {
        fun newInstance(level: Int): DashboardFragment {
            val args = Bundle()
            args.putInt(ARG_LEVEL,level)
            val fragment = DashboardFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_dashboard
    }

    override fun bindScreen() {
        level = arguments?.getInt(ARG_LEVEL)
        dashboardRecyclerV.setHasFixedSize(true)
        dashboardRecyclerV.layoutManager = GridLayoutManager(context,mDashboardPresenter.getSpanCountAccordingToLevel(level!!))
        val adapter = DashboardGridAdapter(context!!,mDashboardPresenter.getListAccordingToLevel(level!!))
        adapter.setGameListener(this)
        dashboardRecyclerV.adapter = adapter
    }

    override fun onGameFinished() {
        if (level == Constants.GameValues.lastLevel){
            BusProvider.getInstance().postDelayed(EndGameEvent(),150)
        }else{
            Handler().postDelayed({
                parentActivity.changeFragment(newInstance(this.level?.plus(1)!!))
            },150)
        }
    }
}