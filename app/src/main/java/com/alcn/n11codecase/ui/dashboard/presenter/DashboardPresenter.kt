package com.alcn.n11codecase.ui.dashboard.presenter

import com.alcn.n11codecase.data.model.response.dashboard.ItemModel
import com.alcn.n11codecase.ui.base.presenter.BasePresenter
import com.alcn.n11codecase.ui.dashboard.view.DashboardView

interface DashboardPresenter<V : DashboardView> : BasePresenter<V> {
    fun getListAccordingToLevel(level: Int): ArrayList<ItemModel>
    fun getSpanCountAccordingToLevel(level: Int): Int
}