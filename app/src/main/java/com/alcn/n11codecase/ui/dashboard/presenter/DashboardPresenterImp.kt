package com.alcn.n11codecase.ui.dashboard.presenter

import com.alcn.n11codecase.data.model.response.dashboard.ItemModel
import com.alcn.n11codecase.data.preferences.PreferenceHelper
import com.alcn.n11codecase.network.api.ApiHelperImp
import com.alcn.n11codecase.ui.base.presenter.BasePresenterImp
import com.alcn.n11codecase.ui.dashboard.view.DashboardView
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DashboardPresenterImp <V : DashboardView> @Inject internal constructor(
    disposable: CompositeDisposable, apiHelperImp: ApiHelperImp, preferenceHelper: PreferenceHelper
) : BasePresenterImp<V>(
    compositeDisposable = disposable,
    apiHelperImp = apiHelperImp,
    preferenceHelper = preferenceHelper
), DashboardPresenter<V> {

    override fun getListAccordingToLevel(level: Int): ArrayList<ItemModel> {
        val levelCount = getCardCountForLevel(level)
        val list = ArrayList<ItemModel>()
        for (n in 1..(levelCount/2)){
            val random1 = (1..10000).shuffled().first()
            val random2 = (1..10000).shuffled().first()
            list.add(ItemModel(random1,n,false,true))
            list.add(ItemModel(random2, n,false,true))
        }
        list.shuffle()
        return list
    }

    override fun getSpanCountAccordingToLevel(level: Int): Int{
        return when (level) {
            1 -> 3
            2 -> 3
            3 -> 4
            4 -> 4
            else -> 3
        }
    }

    private fun getCardCountForLevel(level: Int): Int{
        return when (level) {
            1 -> 6
            2 -> 8
            3 -> 12
            4 -> 16
            else -> 6
        }
    }
}