package com.alcn.n11codecase.ui.main.view

import com.alcn.n11codecase.ui.base.view.BaseView


interface MainView : BaseView {
    fun setTimer(time: Int)
    fun gameOver()
}