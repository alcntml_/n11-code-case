package com.alcn.n11codecase.ui.base.view

interface BaseView {

    fun showProgress()
    fun hideProgress()


}