package com.alcn.n11codecase.ui.main

import com.alcn.n11codecase.ui.main.presenter.MainPresenter
import com.alcn.n11codecase.ui.main.presenter.MainPresenterImp
import com.alcn.n11codecase.ui.main.view.MainView
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    internal fun provideMainPresenter(mainPresenter: MainPresenterImp<MainView>)
            : MainPresenter<MainView> = mainPresenter
}