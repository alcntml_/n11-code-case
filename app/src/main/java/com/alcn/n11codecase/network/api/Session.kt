package com.alcn.n11codecase.network.api

class Session {
    companion object {
        private var currentSession: Session? = Session()
        val current: Session
            get() {
                if (currentSession == null) {
                    currentSession = Session()
                }
                return currentSession as Session
            }
    }
}