package com.alcn.n11codecase.network.api

object ErrorStatus {

    const val SUCCESS = 0

    const val UNKNOWN_ERROR = 1002

    const val SERVER_ERROR = 1003

    const val NETWORK_ERROR = 1004

    const val API_ERROR = 1005

}