package com.alcn.n11codecase.network.api

import com.alcn.n11codecase.data.model.base.BaseResponse
import com.alcn.n11codecase.ui.base.view.BaseActivity
import com.alcn.n11codecase.utility.provider.SchedulerProvider
import io.reactivex.Observable
import javax.inject.Inject

class ApiHelperImp @Inject constructor(
    private val apiHelper: ApiHelper
) {

    fun getHome(baseActivity: BaseActivity): Observable<BaseResponse> {
        return apiHelper.getHome("en").flatMap {
            apiHelper.getHome("en")
        }.compose(SchedulerProvider().ioToMainObservableScheduler())
    }
}